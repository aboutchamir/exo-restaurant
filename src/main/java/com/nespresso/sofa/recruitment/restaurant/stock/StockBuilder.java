package com.nespresso.sofa.recruitment.restaurant.stock;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockBuilder {
	
	private static final String STOCK_ELEMENT_SEPARATOR = " ";
	private static final Integer UNLIMITED_STOCK = Integer.valueOf(-1);
	
	List<String> stockElements ; 
	
	public StockBuilder (String... stockelementsAsArray)
	{
		stockElements = Arrays.asList(stockelementsAsArray) ; 
	}
	
	public Map<String,Integer> buildStockElements () 
	{
		Map<String,Integer> stockElementsAsMap = new HashMap<String,Integer>() ; 
		for (String element : stockElements) {
			Integer stoclLevel    = getStockLevelFromStockElement(element);
			String  ingredientName= getIngeredientNameFromElement(element,stoclLevel);
			stockElementsAsMap.put(ingredientName, stoclLevel);
		}
		return stockElementsAsMap ; 
	}

	private String getIngeredientNameFromElement(String element, Integer stoclLevel) {
		
		if (stoclLevel.equals(UNLIMITED_STOCK))
		{
		   	return element ; 
		}
		String parts [] = element.split(STOCK_ELEMENT_SEPARATOR, 2);
		return parts[1];
	}

	private Integer getStockLevelFromStockElement(String element) {
		Integer stockLeveel = UNLIMITED_STOCK ; 
		String parts [] = element.split(STOCK_ELEMENT_SEPARATOR, 2);
		try {
			stockLeveel = Integer.valueOf(parts[0]);
			
		}
		catch(NumberFormatException e)
		{
			
		}
		return stockLeveel;
	}
	
	

}
