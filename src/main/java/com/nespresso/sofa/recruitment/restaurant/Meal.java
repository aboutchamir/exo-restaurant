package com.nespresso.sofa.recruitment.restaurant;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.receipe.Receipe;

public class Meal {

	private int numberOfServedDishes ; 
	private Map<Receipe,Integer> receipes ; 
	private Map<String,Integer> stock ;

	public Meal(int numberOfServedDishes, Map<String, Integer> stock) {
		
		this.numberOfServedDishes =numberOfServedDishes;
		receipes = new HashMap<>() ; 
		this.stock = stock;
	}

	public Integer servedDishes() {
		
		return Integer.valueOf(numberOfServedDishes);
	}

	public String cookingDuration() {
		int globalCookingDuration = calculateGlobalCookingDuration();
		return String.valueOf(globalCookingDuration);
	}
	
	private int calculateGlobalCookingDuration() {
		int totalCookingDuration = 0 ;
		for (Map.Entry<Receipe,Integer> entry : receipes.entrySet()) {
			Receipe receipe = entry.getKey() ;
			totalCookingDuration = totalCookingDuration + receipe.calculateCookingDuration(entry.getValue());
		}
		return totalCookingDuration;
	}
	
	public void addReceipe (Receipe receipe , int receipeCount ) 
	{   
		receipe.validateAvailabilityprivate(receipeCount, stock);
		receipes.put(receipe, receipeCount);
	}

}
