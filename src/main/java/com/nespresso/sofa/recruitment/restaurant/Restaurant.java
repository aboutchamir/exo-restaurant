package com.nespresso.sofa.recruitment.restaurant;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.receipe.Receipe;
import com.nespresso.sofa.recruitment.restaurant.receipe.ReciepesInitializer;
import com.nespresso.sofa.recruitment.restaurant.stock.StockBuilder;

public class Restaurant {
	
	private Map<String,Integer> stock ; 
	private Map<String,Receipe> availableReceipes ; 
	private Ticket currentTicket  ; 
	
	public Restaurant (String... stockElements)
	{
		StockBuilder stockBuilder = new StockBuilder(stockElements);
		stock = stockBuilder.buildStockElements();
		availableReceipes = ReciepesInitializer.getAvailableReceipes();
	}

	public Ticket order(String order) {
		Ticket ticket = new Ticket(order, availableReceipes);
		currentTicket = ticket;
		return ticket;
	}

	public Meal retrieve(Ticket ticket) {
		return currentTicket.makeAMeal(stock); 
	}
	
	

}
