package com.nespresso.sofa.recruitment.restaurant.receipe;

import java.util.HashMap;
import java.util.Map;

public class ReciepesInitializer {
	
	
	private static final String TOMATO_MOZZARELLA_SALAD_MEAL_NAME = "Tomato Mozzarella Salad";
	private static final int NEEDED_TOMATOS_FOR_SALAD = 2;
	private static final int NEEDED_MOZARELLA_FORS8SALAD = 1;
	private static final String TOMATOES = "tomatoes";
	private static final String BALLS_MOZZARELLA = "balls Mozzarella";
	private static final int STANDARD_COOKING_DURATION_FIR_MOZARELLA_SALAD = 6;
	private static final int SECOND_MEAL_COOKING_DURATION = 3;

	
	
	public static Map<String,Receipe>  getAvailableReceipes() {
		Map<String,Receipe> receipes = new HashMap<>() ; 
		Receipe  mozarellaSaladreceipe = getMozarellaSaladReceipe() ; 
		receipes.put(TOMATO_MOZZARELLA_SALAD_MEAL_NAME,mozarellaSaladreceipe);
		return receipes ; 
	}

	private static Receipe getMozarellaSaladReceipe() {
		
		Map<String,Integer> neededIngredients = new HashMap<>() ; 
		neededIngredients.put(BALLS_MOZZARELLA, NEEDED_MOZARELLA_FORS8SALAD);
		neededIngredients.put(TOMATOES, NEEDED_TOMATOS_FOR_SALAD);
		Receipe receipe = new Receipe(TOMATO_MOZZARELLA_SALAD_MEAL_NAME, neededIngredients , STANDARD_COOKING_DURATION_FIR_MOZARELLA_SALAD,SECOND_MEAL_COOKING_DURATION);
		return receipe;
	}

}
