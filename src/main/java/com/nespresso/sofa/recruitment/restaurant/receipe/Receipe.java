package com.nespresso.sofa.recruitment.restaurant.receipe;

import java.util.Map;
import java.util.Map.Entry;

import com.nespresso.sofa.recruitment.restaurant.UnavailableDishException;

public class Receipe {
	
	private String receipeName ; 
	private Map<String,Integer> neededIngredients;
	private int firstCookingDuration ; 
	private int secondCookingDuration ; 
	
	public Receipe (String receipeName , Map<String,Integer> neededIngredients , int standardCookingDuration ,int secondCookingDuration)
	{
		this.receipeName       			  = receipeName ; 
		this.neededIngredients			  = neededIngredients ;
		this.firstCookingDuration   	  = standardCookingDuration ; 
		this.secondCookingDuration  	  = secondCookingDuration;
	}

	public int getStandardCookingDuration() {
		return firstCookingDuration;
	}
	
	public void validateAvailabilityprivate (int numberOfDishes , Map<String,Integer> stock ){
		for (Map.Entry<String, Integer> entry : neededIngredients.entrySet()) {
			validateIngredientAvailability(entry,numberOfDishes,stock);
		}
	   
	}

	private void validateIngredientAvailability(Entry<String, Integer> entry, int numberOfDishes, Map<String, Integer> stock) {
		
		int neededLevel      = numberOfDishes * entry.getValue() ; 
		int availableInstock = stock.get(entry.getKey());
		if(neededLevel > availableInstock)
		{
			throw new UnavailableDishException();
		}
	}

	public int calculateCookingDuration(int mealCount) {
		
		int globalCookingDuration = firstCookingDuration + sumsecondCookingDurations(mealCount) ;
		return globalCookingDuration;
	}

	private int sumsecondCookingDurations(int mealCount) {
		int subsequentDishesCookingDuration = 0 ; 
		if(mealCount >= 2 )
		{   
			int numberOfSubsequentDishes = mealCount - 1;
			subsequentDishesCookingDuration = numberOfSubsequentDishes  * secondCookingDuration ;
			return subsequentDishesCookingDuration;
		}
		return subsequentDishesCookingDuration;
	}
	

}
