package com.nespresso.sofa.recruitment.restaurant;

import java.util.Map;

import com.nespresso.sofa.recruitment.restaurant.receipe.Receipe;

public class Ticket {
    
	private static final String ORDER_SEPARATOR = " ";
	private String  mealName ;
	private int mealCount; 
	private Map<String,Receipe> availableReceipes ;
	private Meal currentMeal ; 
	
	public Ticket (String order,Map<String,Receipe> availableReceipes)
	{
		this.mealName =  getMealName(order) ; 
		this.mealCount =  getMealCount(order) ; 
		this.availableReceipes =availableReceipes;
	}

	public Ticket and(String string) {
		
		return this;
	}

	public Meal makeAMeal(Map<String, Integer> stock) {
		Receipe receipe = availableReceipes.get(mealName);
		Meal meal = new Meal(mealCount,stock) ; 
		meal.addReceipe(receipe, mealCount);
		currentMeal = meal ; 
		return meal;
	}
	
	private int getMealCount(String order) {
		String orderParts [] = order.split(ORDER_SEPARATOR,2);
		Integer mealCount = Integer.valueOf(orderParts[0]);
		return mealCount;
	}

	private String getMealName(String order) {
		String orderParts [] = order.split(ORDER_SEPARATOR,2);
		return orderParts[1];
	}

}
